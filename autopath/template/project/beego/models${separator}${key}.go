package models

import (
	"github.com/astaxie/beego/logs"
	"github.com/astaxie/beego/orm"
	"strconv"
	"web/component/base"
	"web/utils"
)

type @{crud.table.className} struct {
	// columns START
	# for(column in crud.table.columns){ #
	# if(column.columnName=='enable'||column.columnName=='update_time'||column.columnName=='update_id'||column.columnName=='create_time'||column.columnName=='create_id'){ #
	# 	continue; #
	# } #
	@{strutils.toUpperCaseFirst(column.columnJavaName)}		@{strutils.toLowerCase(column.goType)} `form:"@{strutils.toLowerCaseFirst(column.columnJavaName)}" json:"@{strutils.toLowerCaseFirst(column.columnJavaName)}"`  // @{column.remarks}
	# } #
	// columns END

	base.BaseModel
}


func (model @{crud.table.className}) Get(id int) @{crud.table.className} {
	if model.Id > 0 {
		id = model.Id
	}

	getModel := @{crud.table.className}{}
	o := orm.NewOrm()
	err := o.Raw("SELECT " + model.getColumns() + " FROM " + model.getTableName() + " t WHERE id = ?", id).QueryRow(&getModel)
	if err != nil {
		logs.Error("@{crud.table.className} get error", err)
	}

	return getModel
}


func (model @{crud.table.className}) List(form base.BaseForm) []@{crud.table.className} {
	listModel := []@{crud.table.className}{}
	where := " WHERE 1=1 "
	o := orm.NewOrm()
	_, err := o.Raw("SELECT " + model.getColumns() + " FROM " + model.getTableName() + " t " + where).QueryRows(&listModel)
	if err != nil {
		logs.Error("@{crud.table.className} get error", err)
	}

	return listModel
}

func (model @{crud.table.className}) Paginator(form base.BaseForm) ([]@{crud.table.className}, utils.Paginator) {
	listModel := []@{crud.table.className}{}
	start := 0
	page := 20
	where := " WHERE 1=1 "
	o := orm.NewOrm()

	sql := "SELECT " + model.getColumns() + " FROM " + model.getTableName() + " t " + where + " LIMIT  " + strconv.Itoa(start) + "," + strconv.Itoa(page)
	logs.Info(sql)
	_, err := o.Raw(sql).QueryRows(&listModel)
	if err != nil {
		logs.Error("@{crud.table.className} list error", err)
		return listModel, utils.Paginator{}
	}

	countSql := "SELECT count(1) as cnt FROM " + model.getTableName() + " " + where

	var maps []orm.Params
	num, err := o.Raw(countSql).Values(&maps)
	if err != nil || num != 1 {
		logs.Error("@{crud.table.className} countSql error", err)
		return listModel, utils.Paginator{}
	}
	cnt := maps[0]["cnt"].(string)

	paginator := utils.Paginator{}
	paginator.SetNums(cnt)

	return listModel, paginator
}

func (model @{crud.table.className}) Delete(id int) int64 {
	if model.Id > 0 {
		id = model.Id
	}

	o := orm.NewOrm()
	res, err := o.Raw("DELETE FROM " + model.getTableName() + " WHERE id = ?", id).Exec()
	if err != nil {
		logs.Error("@{crud.table.className} delete error", err)
		return 0
	}

	num, _ := res.RowsAffected()
	return num
}

#
var updateStr = "";
for(column in crud.table.columns){
if(columnLP.index!=1) {
updateStr = updateStr + ",";
}
updateStr = updateStr + strutils.toLowerCase(column.columnName) + "=?";
}
#
func (model @{crud.table.className}) Update() int64 {
	o := orm.NewOrm()

	#
	var updateParams = "";
	var updateValues = "";
	var updateFirst = 1;
	for(column in crud.table.columns){

	if(column.pk){
	updateFirst = updateFirst + 1;
	continue;
	}

	if(columnLP.index!=updateFirst) {
	updateParams = updateParams + ",";
	updateValues = updateValues + ",";
	}

	updateParams = updateParams + "model." + strutils.toUpperCaseFirst(column.columnJavaName);
	updateValues = updateValues + strutils.toLowerCase(column.columnName) + "=?";
	}
	#

	var updateValues = "@{updateValues}";
	updateParams := []interface{}{@{updateParams}}

	res, err := o.Raw("UPDATE " + model.getTableName() + " SET "+updateValues,updateParams).Exec()
	if err != nil {
		logs.Error("@{crud.table.className} update error", err)
		return 0
	}

	num, _ := res.RowsAffected()
	return num
}

func (model @{crud.table.className}) Insert() int64 {
	o := orm.NewOrm()

	#
	var insertParams = "";
	var insertValues = "";
	var insertMarks = "";
	var insertFirst = 1;
	for(column in crud.table.columns){

	if(column.pk){
		insertFirst = insertFirst + 1;
		continue;
	}

	if(columnLP.index!=insertFirst) {
	insertParams = insertParams + ",";
	insertValues = insertValues + ",";
	insertMarks = insertMarks + ",";
	}
	insertParams = insertParams + "model." + strutils.toUpperCaseFirst(column.columnJavaName);
	insertValues = insertValues + strutils.toLowerCase(column.columnName);
	insertMarks = insertMarks + "?";
	}
	#

	var insertValues = "@{insertValues}";
	var insertMarks = "@{insertMarks}";
	params := []interface{}{@{insertParams}}

	res, err := o.Raw("INSERT INTO " + model.getTableName() + " ("+insertValues+") VALUES ("+insertMarks+")",params).Exec()
	if err != nil {
		logs.Error("@{crud.table.className} inser error", err)
		return 0
	}

	num, _ := res.RowsAffected()
	return num
}

func (model @{crud.table.className}) getTableName() string{
	return "@{crud.table.tableName}"
}

func (model @{crud.table.className}) getColumns() string{
	#
	var str = "";
	for(column in crud.table.columns){
	if(columnLP.index!=1) {
	str = str + ",";
	}
	str = str + "t." + strutils.toLowerCase(column.columnName);
	if(strutils.toLowerCase(column.columnName)!=strutils.toLowerCaseFirst(column.columnJavaName) ) {
	str = str + " as " + strutils.toLowerCaseFirst(column.columnJavaName);
	}
	}
	#
	sqlColumns := "@{str}"
	return sqlColumns
}
