package @{module}

import (
	"web/component/base"
	"web/models"
	"web/utils"
)

/**
	// @{crud.table.remarks} 管理
	beego.Router("/system/@{crud.urlKey}/index", &@{module}.@{strutils.toUpperCaseFirst(crud.urlKey)}Controller{}, "get:Index")
	beego.Router("/system/@{crud.urlKey}/get/:id", &@{module}.@{strutils.toUpperCaseFirst(crud.urlKey)}Controller{}, "*:Get")
	beego.Router("/system/@{crud.urlKey}/delete/:id", &@{module}.@{strutils.toUpperCaseFirst(crud.urlKey)}Controller{}, "*:Delete")
	beego.Router("/system/@{crud.urlKey}/save", &@{module}.@{strutils.toUpperCaseFirst(crud.urlKey)}Controller{}, "*:Save")
	beego.Router("/system/@{crud.urlKey}/list", &@{module}.@{strutils.toUpperCaseFirst(crud.urlKey)}Controller{}, "*:List")
	beego.Router("/system/@{crud.urlKey}/paginator", &@{module}.@{strutils.toUpperCaseFirst(crud.urlKey)}Controller{}, "*:Paginator")
 */
type @{strutils.toUpperCaseFirst(crud.urlKey)}Controller struct {
	base.BaseRouter
}

// path: /index
func (this *@{strutils.toUpperCaseFirst(crud.urlKey)}Controller) Index() {
	this.TplName = "pages/@{module}/@{crud.urlKey}/@{crud.urlKey}_index.tpl"
}

// path: /get/{id}
func (this *@{strutils.toUpperCaseFirst(crud.urlKey)}Controller) Get() {
	id, _ := this.GetInt(":id")
	@{crud.urlKey} := models.@{crud.table.className}{}.Get(id)

	resp := utils.Resp{}.Succ(@{crud.urlKey})

	this.Data["json"] = resp
	this.ServeJSON()
}

// path: /delete/{id}
func (this *@{strutils.toUpperCaseFirst(crud.urlKey)}Controller) Delete() {
	id, _ := this.GetInt(":id")

	num := models.@{crud.table.className}{}.Delete(id)
	resp := utils.Resp{}.Succ("")
	if num <= 0 {
		resp = utils.Resp{}.Fail("update fail");
	}

	this.Data["json"] = resp
	this.ServeJSON()
}

// path: /save
func (this *@{strutils.toUpperCaseFirst(crud.urlKey)}Controller) Save() {
	model := models.@{crud.table.className}{}
	err := this.ParseForm(&model)

	if err != nil {
		this.Data["json"] = utils.Resp{}.Error(err.Error())
		this.ServeJSON()
		return
	}

	userId := this.GetSession("uid").(int)

	resp := utils.Resp{}.Fail("save fail");
	model.UpdateId = userId
	model.UpdateTime = utils.GetNow()

	// insert
	if model.Id <= 0 {
		model.CreateId = userId
		model.CreateTime = utils.GetNow()
		num := model.Insert()
		if num > 0 {
			resp = utils.Resp{}.Succ("")
		}
	} else {
		// update
		// old@{crud.urlKey} := models.@{crud.table.className}{}.Get(model.Id)

		num := model.Update()
		if num > 0 {
			resp = utils.Resp{}.Succ("")
		}
	}

	this.Data["json"] = resp
	this.ServeJSON()
}

// path: /list
func (this *@{strutils.toUpperCaseFirst(crud.urlKey)}Controller) List() {
	form := base.BaseForm{}
	err := this.ParseForm(&form)
	if err != nil {
		this.Data["json"] = utils.Resp{}.Error(err.Error())
		this.ServeJSON()
		return
	}

	list := models.@{crud.table.className}{}.List(form)
	resp := utils.Resp{}.Succ(list)

	this.Data["json"] = resp
	this.ServeJSON()
}

// path: /paginator
func (this *@{strutils.toUpperCaseFirst(crud.urlKey)}Controller) Paginator() {
	form := base.BaseForm{}
	err := this.ParseForm(&form)
	if err != nil {
		this.Data["json"] = utils.Resp{}.Error(err.Error())
		this.ServeJSON()
		return
	}
	list, _ := models.@{crud.table.className}{}.Paginator(form)
	resp := utils.Resp{}.Succ(list)

	this.Data["json"] = resp
	this.ServeJSON()
}
