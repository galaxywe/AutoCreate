package @{packagePath}.action;

import com.channelsoft.datacenter.component.base.BaseAdminController;
import com.channelsoft.datacenter.component.model.Query;
import com.github.pagehelper.PageInfo;
import com.channelsoft.datacenter.component.model.resubmit.SameUrlData;
import @{packagePath}.model.@{crud.table.className};
import @{packagePath}.service.I@{strutils.toUpperCaseFirst(crud.urlKey)}Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.Map;

/**
 * @{crud.table.remarks} 控制层
 *
 * @author flyfox 369191470@qq.com on @{now}.
 */
@RestController
@RequestMapping(value = "/@{module}/@{crud.urlKey}")
public class @{strutils.toUpperCaseFirst(crud.urlKey)}Controller extends BaseAdminController{

    @Autowired
    private I@{strutils.toUpperCaseFirst(crud.urlKey)}Service service;

    private static final String path = "/pages/@{module}/@{crud.urlKey}/@{crud.urlKey}_";

    @RequestMapping(value = "/index")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView(path + "index.html");
        return view;
    }

    @RequestMapping(value = "/get/{id}")
    public Object get(@PathVariable Long id) {
        @{crud.table.className} model = service.selectById(id);
        return success(model);
    }

    @SameUrlData
    @RequestMapping(value = "/delete/{id}")
    public Object delete(@PathVariable Long id) {
        @{crud.table.className} model = new @{crud.table.className}();
        Long userid = getSessionUser().getId();
        String now = getNow();
        model.setUpdateId(userid);
        model.setUpdateTime(now);
        model.setId(id);

        if (!service.deleteByIdLog(model)) {
            return fail("删除失败");
        }

        return success();
    }

    @SameUrlData
    @RequestMapping(value = "/save")
    public Object save(@ModelAttribute @{crud.table.className} model) {
        Long id = model.getId();
        Long userid = getSessionUser().getId();
        String now = getNow();
        model.setUpdateId(userid);
        model.setUpdateTime(now);
        if (id!=null&&id>0) { // 更新
            service.updateByIdLog(model);
        } else { // 新增
            model.setCreateId(userid);
            model.setCreateTime(now);
            service.insertLog(model);
        }

        return success();
    }

    @RequestMapping(value = "/jqgrid")
    public Object jqgrid(@RequestParam Map<String, Object> params) {
        //查询列表数据
        Query query = new Query(params);
        PageInfo<@{crud.table.className}> pageData = service.select@{strutils.toUpperCaseFirst(crud.urlKey)}Page(query);
        return getJqgridList(pageData);
    }

}