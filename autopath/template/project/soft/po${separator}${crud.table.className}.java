package @{packagePath}.po;

import com.channelsoft.ccuas.web.commom.base.BasePo;

public class @{crud.table.className} extends BasePo {

	private static final long serialVersionUID = 1L;

    // columns START
	# for(column in crud.table.columns){ #

		# if(column.pk) { #
		# } else { #
			# if(strutils.toLowerCase(column.columnName)!=strutils.toLowerCaseFirst(column.columnJavaName) ) {#
			# } #
		# } #
	private @{column.javaType} @{strutils.toLowerCaseFirst(column.columnJavaName)};  // @{column.remarks}
    # } #
	// columns END

    # for(column in crud.table.columns){ #

	public @{column.javaType} get@{strutils.toUpperCaseFirst(column.columnJavaName)}() {
		return @{strutils.toLowerCaseFirst(column.columnJavaName)};
	}

    public void set@{strutils.toUpperCaseFirst(column.columnJavaName)}(@{column.javaType} @{strutils.toLowerCaseFirst(column.columnJavaName)}) {
    	this.@{strutils.toLowerCaseFirst(column.columnJavaName)} = @{strutils.toLowerCaseFirst(column.columnJavaName)};
    }
	# } #
	
	@Override
	public String toString() {
		String log = ""; 
	# for(column in crud.table.columns){ #
		log += "[@{strutils.toLowerCaseFirst(column.columnJavaName)}:" + get@{strutils.toUpperCaseFirst(column.columnJavaName)}() + "]";
	# } #
		log += super.toString();
		return log;
	}
}