package @{packagePath}.mapper;

import @{packagePath}.po.@{crud.table.className};
import org.apache.ibatis.annotations.*;
import org.apache.ibatis.jdbc.SQL;

import java.util.List;

/**
 * @{crud.table.remarks} 数据层
 *
 * @author flyfox 369191470@qq.com on @{now}.
 */
public interface @{strutils.toUpperCaseFirst(crud.urlKey)}Mapper {

    /**
     * 获取@{crud.table.remarks}对象
     *
     * @param id
     * @return
     */
    @Select("SELECT * FROM `@{crud.table.tableName}` t WHERE id=\#{id,jdbcType=INTEGER}")
    @{crud.table.className} selectOne(@Param("id") Integer id);

    /**
     * 获取@{crud.table.remarks}对象列表
     * @return
     */
    @SelectProvider(type = SqlBuilder.class, method = "selectList")
    List<@{crud.table.className}> selectList(@Param("model") @{crud.table.className} model);

    /**
     * 新增@{crud.table.remarks}信息
     *
     * @param model
     * @return
     */
    @InsertProvider(type = SqlBuilder.class, method = "insert")
    @Options(useGeneratedKeys = true, keyProperty = "model.id")
    int insert(@Param("model") @{crud.table.className} model);

    /**
     * 更新@{crud.table.remarks}信息
     *
     * @param model
     * @return
     */
    @UpdateProvider(type = SqlBuilder.class, method = "update")
    int update(@Param("model") @{crud.table.className} model);

    /**
     * 根据主键删除
     *
     * @param id
     * @return
     */
    @Delete("DELETE FROM `@{crud.table.tableName}` WHERE id=\#{id}")
    int delete(@Param("id") Integer id);

    /**
     * sql构建
     */
    class SqlBuilder {

        public String selectList(final @{crud.table.className} model) {
            // return sql;
            return new SQL() {{
                SELECT("*");
                FROM("`@{crud.table.tableName}` t");
            }}.toString();
        }

        public String update(final @{crud.table.className} model) {
            return new SQL(){{
                UPDATE("@{crud.table.tableName}");
                # for(column in crud.table.columns){  #
                    # if(column.pk) { #
                    # } else { #
                        if(model.get@{strutils.toUpperCaseFirst(column.columnJavaName)}() != null){
                            SET("@{strutils.toLowerCase(column.columnName)}=\#{model.@{strutils.toLowerCaseFirst(column.columnJavaName)}}");
                        }
                    # } #
                #  } #
                WHERE("id=\#{model.id}");
            }}.toString();
        }

        public String insert(final @{crud.table.className} model) {
            return new SQL(){{
                INSERT_INTO("@{crud.table.tableName}");
                 # for(column in crud.table.columns){  #
                    if(model.get@{strutils.toUpperCaseFirst(column.columnJavaName)}() != null){
                        VALUES("@{strutils.toLowerCase(column.columnName)}","\#{model.@{strutils.toLowerCaseFirst(column.columnJavaName)}}");
                    }
                #  } #
            }}.toString();
        }
    }

}