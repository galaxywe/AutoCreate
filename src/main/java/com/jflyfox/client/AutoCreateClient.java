package com.jflyfox.client;

import com.jflyfox.autocreate.template.CRUD;
import com.jflyfox.autocreate.util.AutoCreate;
import com.jflyfox.autocreate.util.DbUtils;
import com.jflyfox.util.Config;
import com.jflyfox.util.StrUtils;

import java.sql.SQLException;
import java.util.Map;

public class AutoCreateClient {

	public static void main(String[] args) throws Exception {
		run();
	}

	protected static void run() throws SQLException, Exception {
		DbUtils.init();
		// 模块名
		String module = "system";
		// 包名
		String packagePath = Config.getStr("template.packagePath");
		// 生成的表，支持逗号分割
		String tables = Config.getStr("template.tables");
		// 模板路径
		String selected = Config.getStr("template.selected");
		String templatePath = Config.getStr(selected);

		Map<String, CRUD> crudMap = null;
		if (StrUtils.isEmpty(tables) || "all".equalsIgnoreCase(tables)) {
			crudMap = DbUtils.getCRUDMap();
		} else {
			String[] tableArray = tables.split(",");
			crudMap = DbUtils.getCRUDMap(tableArray);
		}

		// System.setProperty("user.dir","D:\\workspace\\datacenter\\wyx_component\\autocreate");
		new AutoCreate().setTemplatePath(templatePath).setPackagePath(packagePath).setModule(module).setCrudMap(crudMap).create();

	}

}
